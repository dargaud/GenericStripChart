///////////////////////////////////////////////////////////////////////////////
// MODULE	GenericStripChart.c
// PURPOSE	Creates one or more panels with sets of variables displayed on strip charts
// NOTE		Requires the GenericStripChartUI.uir/h files
// AUTHOR	(c) 2012 Guillaume Dargaud - http://www.gdargaud.net/Hack/LabWindows.html
///////////////////////////////////////////////////////////////////////////////


#include <stdlib.h>		

#include <cvirte.h>		
#include <userint.h>
#include <toolbox.h>


#include "Def.h"
#include "GenericStripChartUI.h"
#include "GenericStripChart.h"

#include "ShowAttributes.h"	// FIXME - Remove that

///////////////////////////////////////////////////////////////////////////////
// User settable options

//#define GSC_DEBUG	// Compile as standalone test, see the main() at the end of this file
#define USE_TSL		// Optional: Use time strip labels.
#define USE_PM		// Optional: Panel management routines (printing, etc)
#define USE_EASP	// Optional: Edit axis settings popup
// See http://www.gdargaud.net/Hack/LabWindows.html for the source code to those 3 USE options

#define MAX_TRACES 16	// That's the total but also the max per chart
						// The max accepted by strip charts is actually 512
						
#define MAX_STRIPS 8	// Limited by the height of the panel. Impractical to have too many

// There's no limit to the number of variables

///////////////////////////////////////////////////////////////////////////////

#ifdef USE_TSL
	#include "TimeStripLabel.h"
#endif
#ifdef USE_PM
	#include "PanelManagement.h"
#endif
#ifdef USE_EASP
	#include "EditAxisSettingsPopup.h"
#endif

int NoTimePlots=FALSE;	// Debug only - FIXME

///////////////////////////////////////////////////////////////////////////////

// One of the possible variables to trace
typedef struct sVar {
	double *Pt;
	char *Label;
} tVar;

// One of the possible traces
typedef struct sTrace {
	int Axis;		// even for left, odd for right, /2 for strip index, same value as the Axis ctrl
	double *Pt;		// Same as one of tVar
	int CtrlAxis, CtrlSelect;
} tTrace;


// One of the possible panels where the generic strip charts are loaded
typedef struct sGscPanel {
	int Panel, 							// Panel handle when multiple
		NbTraces, 						// Number of active traces
		PPS, 							// Number of points per strip chart (3..10000)
		NbVars,							// Number of defined variables
		ScrollMode;						// Scrolling mode (0..2)
//		Fixed;							// Fixed plot area - not easy as a saveable parameter
	double Rate;
	tTrace TraceList[MAX_TRACES];		// List of active traces (0..NbTraces-1)
	int IndexOfCtrl[MAX_TRACES*3+20];	// Match TraceList indices or Strips indices with Axis/Select/Strip controls
	int CtrlTop, CtrlHeight;			// Of the 1st Axis/Select
	ListType Variables;					
	int NbStrips;						// At least one
	int Strips[MAX_STRIPS];				// UI control number
	int NbTracesOnStrip[MAX_STRIPS];	// Number of traces on a given strip chart
	BOOL Customize;						// Show more controls
	#ifdef USE_TSL
	int Unit;							// For time strip labels
	#endif
	BOOL NotYet;
} tGscPanel;

static int NbPanels=0;				// Number of GSC panels in use
static tGscPanel *GscPanels=NULL;	// Array of size NbPanels


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find an already configured GSC panel, or add it to the list and configure it otherwise
/// HIPAR	panel/Refers to an already configured or new GSC panel
/// HIRET	NULL if the panel is not a GSC panel or there's an error
///////////////////////////////////////////////////////////////////////////////
static tGscPanel* GSC_Find(int panel) {
	int i, LeftAxis, LeftSelect;
	char Str[2];
	tGscPanel *P;
	
	if (NoTimePlots) return NULL;
	for (i=0; i<NbPanels; i++)
		if (GscPanels[i].Panel==panel)
			return &GscPanels[i];	// We found it already configured

	// Sanity check, make sure we are dealing with a GSC panel
	GetCtrlAttribute (panel, GSC_STRIP,     ATTR_CTRL_STYLE, &i); if (i!=CTRL_STRIP_CHART_LS)        return NULL;
	GetCtrlAttribute (panel, GSC_NB_TRACES, ATTR_CTRL_STYLE, &i); if (i!=CTRL_NUMERIC_LS)            return NULL;
	GetCtrlAttribute (panel, GSC_AXIS,      ATTR_CTRL_STYLE, &i); if (i!=CTRL_RECESSED_MENU_RING_LS) return NULL;

	// New GSC panel - Set default values
	if (NULL==(GscPanels=realloc(GscPanels, (size_t)(++NbPanels)*sizeof(tGscPanel)))) return NULL;
	P=&GscPanels[NbPanels-1];
	P->Panel  =panel;
	P->NbTraces=0;
	P->PPS     =100;
	P->Rate    =1.;
	P->NbVars  =0;
	P->Variables=ListCreate(sizeof(tVar));
	P->NbStrips=0;
	P->Customize=TRUE;
	P->NotYet=TRUE;
	
	GetCtrlAttribute(panel, GSC_AXIS,  ATTR_HEIGHT,&P->CtrlHeight);
	GetCtrlAttribute(panel, GSC_AXIS,  ATTR_TOP,   &P->CtrlTop);
	GetCtrlAttribute(panel, GSC_AXIS,  ATTR_LEFT,  &LeftAxis);
	GetCtrlAttribute(panel, GSC_SELECT,ATTR_LEFT,  &LeftSelect);
	for (i=0; i<MAX_TRACES; i++) {
		tTrace *TL=&P->TraceList[i];
		P->IndexOfCtrl[ TL->CtrlAxis  =DuplicateCtrl(panel, GSC_AXIS,   panel, "", P->CtrlTop+i*P->CtrlHeight, LeftAxis) ] =
		P->IndexOfCtrl[ TL->CtrlSelect=DuplicateCtrl(panel, GSC_SELECT, panel, "", P->CtrlTop+i*P->CtrlHeight, LeftSelect) ] =
			i;
		TL->Axis=0;
		TL->Pt  =NULL;
	}
	DiscardCtrl(panel, GSC_AXIS);
	DiscardCtrl(panel, GSC_SELECT);
	#undef GSC_AXIS
	#undef GSC_SELECT

	for (i=0; i<MAX_STRIPS; i++) {
		P->NbTracesOnStrip[i]=0;
		sprintf(Str, "%c", 'A'+i);
		P->IndexOfCtrl[ P->Strips[i] = DuplicateCtrl(panel, GSC_STRIP, panel, Str, i*50, 0) ] =
		i;
	}
	DiscardCtrl(panel, GSC_STRIP);
	#undef GSC_STRIP
	
	SetCtrlAttribute (panel, GSC_NB_STRIPS, ATTR_ZPLANE_POSITION, 0);
	SetCtrlAttribute (panel, GSC_NB_TRACES, ATTR_ZPLANE_POSITION, 0);
	SetCtrlAttribute (panel, GSC_FILL,      ATTR_ZPLANE_POSITION, 0);
	SetCtrlAttribute (panel, GSC_NOVAR,     ATTR_ZPLANE_POSITION, 0);

	#ifdef USE_TSL
	P->Unit=TSL_NONE;
	#endif
	
	#ifdef USE_EASP
	char St2[500];
	GetCtrlVal(panel, GSC_TEXTMSG, St2);
	strcat(St2, "\nRight-click on axis selectors to change axis properties.");
	SetCtrlVal(panel, GSC_TEXTMSG, St2);
	#endif

	return P;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init or change the settings on a GSC panel
/// HIFN	Call this after loading the GenericStripChartUI.uir file into a panel or tab
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	NbStrips/Number of strip charts at the start (1 by default)
/// HIPAR	NbTraces/Number of traces at the start (0 or 1 by default)
/// HIPAR	PPS/Number of points per screen
/// HIPAR	Rate/Number of updates per second (0 as fast as possible)
/// HIPAR	ScrollMode/0 sweep, 1 continuous, 2 block
/// HIPAR	Fixed/1 for fixed plot area so they align vertically
/// HIRET	0 if no error, 1 if the panel is not a valid GSC panel
///////////////////////////////////////////////////////////////////////////////
int GSC_SetProperties(int panel, int NbStrips, int NbTraces, int PPS, double Rate, 
					  int ScrollMode /*, int Fixed*/) {
	tGscPanel *P=GSC_Find(panel);
	if (P==NULL) return 1;

	SetCtrlAttribute (panel, GSC_NB_STRIPS,  ATTR_MIN_VALUE, 1);
	SetCtrlAttribute (panel, GSC_NB_STRIPS,  ATTR_MAX_VALUE, MAX_STRIPS);
	SetCtrlAttribute (panel, GSC_NB_TRACES,  ATTR_MIN_VALUE, 0);
	SetCtrlAttribute (panel, GSC_NB_TRACES,  ATTR_MAX_VALUE, MAX_TRACES);
	
	SetCtrlAttribute (panel, GSC_NB_STRIPS,  ATTR_CTRL_VAL, P->NbStrips=NbStrips);
	SetCtrlAttribute (panel, GSC_NB_TRACES,  ATTR_CTRL_VAL, P->NbTraces=NbTraces);
	SetCtrlAttribute (panel, GSC_PPS,        ATTR_CTRL_VAL, P->PPS=PPS);
	SetCtrlAttribute (panel, GSC_RATE,       ATTR_CTRL_VAL, P->Rate=Rate);
	SetCtrlAttribute (panel, GSC_SCROLL_MODE,ATTR_CTRL_VAL, P->ScrollMode=ScrollMode);
//	SetCtrlAttribute (panel, GSC_ALIGN,      ATTR_CTRL_VAL, P->Fixed=Fixed);

	CallCtrlCallback (panel, GSC_NB_STRIPS,  EVENT_COMMIT,     0, 0, NULL);
	CallCtrlCallback (panel, GSC_NB_TRACES,  EVENT_COMMIT,     0, 0, NULL);
	CallCtrlCallback (panel, GSC_PPS,        EVENT_COMMIT,     0, 0, NULL);
	CallCtrlCallback (panel, GSC_RATE,       EVENT_COMMIT,     0, 0, NULL);
	CallCtrlCallback (panel, GSC_SCROLL_MODE,EVENT_COMMIT,     0, 0, NULL);
	
	CallPanelCallback(panel,                 EVENT_PANEL_SIZE, 0, 0, NULL);

//	if (Fixed) {
//		SetCtrlAttribute (panel, GSC_ALIGN,      ATTR_CTRL_VAL, FALSE);
//		CallCtrlCallback (panel, GSC_ALIGN,      EVENT_COMMIT,     0, 0, NULL);
//		SetCtrlAttribute (panel, GSC_ALIGN,      ATTR_CTRL_VAL, P->Fixed=Fixed);
//		CallCtrlCallback (panel, GSC_ALIGN,      EVENT_COMMIT,     0, 0, NULL);
//	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Place the help in a readable place
///////////////////////////////////////////////////////////////////////////////
void PlaceTextMsg(tGscPanel *P) {
	int Width, L1, L2, W, L, L3;
	BOOL T=FALSE;
	GetPanelAttribute(P->Panel, ATTR_WIDTH, &Width);

	GetCtrlAttribute(P->Panel, GSC_ALIGN, ATTR_LABEL_LEFT,  &L1);
	GetCtrlAttribute(P->Panel, GSC_ALIGN, ATTR_LABEL_WIDTH, &W); L1+=W;
	GetCtrlAttribute(P->Panel, GSC_FILL,  ATTR_LEFT,        &L2);
	GetCtrlAttribute(P->Panel, GSC_FILL,  ATTR_WIDTH,       &W); L2+=W+2;
	L=MAX(L1, L2);
	GetCtrlAttribute(P->Panel, GSC_NB_STRIPS,  ATTR_LEFT,   &L3);

	if (Width>L+50)     T=TRUE;
	if (P->NbTraces>=6) T=FALSE;
	SetCtrlAttribute(P->Panel, GSC_TEXTMSG, ATTR_LEFT, T ? L  : L3);
	SetCtrlAttribute(P->Panel, GSC_TEXTMSG, ATTR_TOP,  T ? 1  : 45);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Adjust the width of the variable selection controls
/// HIFN	Call this when the list of variables grows or shrinks or when a variable is renamed
/// HIPAR   P/GSC panel structure
///////////////////////////////////////////////////////////////////////////////
static void AdjustSelectWidth(tGscPanel *P) {
	int i, Left, Width;

	// This function is incredibly slow, so we move it someplace where it's called only once
	for (i=0; i<MAX_TRACES; i++) 
		if (!P->NotYet and 
			0!=SizeRingCtrlToText(P->Panel, P->TraceList[i].CtrlSelect)) 
				return;
	// NOTE: SizeRingCtrlToText has a bug in CVI<=2010, be sure to modify toolbox.c
	//       See http://forums.ni.com/t5/LabWindows-CVI/SizeRingCtrlToText-not-working/m-p/1916237/highlight/true
	
	GetCtrlAttribute (P->Panel, P->TraceList[0].CtrlSelect, ATTR_LEFT,  &Left);
	GetCtrlAttribute (P->Panel, P->TraceList[0].CtrlSelect, ATTR_WIDTH, &Width);
	
	SetCtrlAttribute(P->Panel, GSC_COPYRIGHT, ATTR_LEFT,         Left+=Width+5);
	SetCtrlAttribute(P->Panel, GSC_NB_STRIPS, ATTR_LEFT,         Left);
	GetCtrlAttribute(P->Panel, GSC_NB_STRIPS, ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GSC_NB_STRIPS, ATTR_LABEL_WIDTH, &Width);
	SetCtrlAttribute(P->Panel, GSC_NB_TRACES, ATTR_LEFT,         Left+=Width+5);
	GetCtrlAttribute(P->Panel, GSC_NB_TRACES, ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GSC_NB_TRACES, ATTR_LABEL_WIDTH, &Width);
	SetCtrlAttribute(P->Panel, GSC_FILL,      ATTR_LEFT,         Left+=Width+5);
	PlaceTextMsg(P);
}

///////////////////////////////////////////////////////////////////////////////
// Compares the variable pointers of list elements
static int CVICALLBACK CompareVarPt(void *item1, void *item2) {
	tVar *I1=item1, *I2=item2;
	return I2->Pt - I1->Pt;	// Test for NULL ?
}

// Compares the strings of list elements
static int CVICALLBACK CompareVarLabel(void *item1, void *item2) {
	tVar *I1=item1, *I2=item2;
	return strcmp(I1->Label, I2->Label);	// Test for NULL ?
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Adjust trace numbers on each chart and rebuild legends
/// HIFN	Call this if there's any change to the traces on any strip
/// HIPAR	P/Points to a GSC panel structure
//  NOTE	We try to keep chart resets to a minimum, in consequence they may desynchronize
//			Another (cleaner) way would be to reset them all each time something changes
///////////////////////////////////////////////////////////////////////////////
static void RebuildCharts(tGscPanel *P) {
	tTrace *Tr;
	tVar *V=NULL, VV={NULL, NULL};
	int i, j, Strip, UseLeft, UseRight, NewNb, PrevNb;
	size_t Pos;
	
	for (j=0; j<MAX_STRIPS; j++) P->NbTracesOnStrip[j]=0;

	if (P->NbVars==0) return;	// Nothing to do
	
	for (i=0; i<P->NbTraces; i++) {
		Tr=&P->TraceList[i];
		Strip=Tr->Axis/2;
		P->NbTracesOnStrip[Strip]++;
	}
	for (j=0; j<P->NbStrips; j++) {
		NewNb= (P->NbTracesOnStrip[j]>0 ? P->NbTracesOnStrip[j] : 1);
		SetCtrlAttribute (P->Panel, P->Strips[j], ATTR_DIMMED, P->NbTracesOnStrip[j]==0);
		GetCtrlAttribute (P->Panel, P->Strips[j], ATTR_NUM_TRACES, &PrevNb);
		SetCtrlAttribute (P->Panel, P->Strips[j], ATTR_NUM_TRACES,  NewNb);
		SetCtrlAttribute (P->Panel, P->Strips[j], ATTR_LEGEND_NUM_VISIBLE_ITEMS, P->NbTracesOnStrip[j]);
		SetCtrlAttribute (P->Panel, P->Strips[j], ATTR_LEGEND_VISIBLE,           P->NbTracesOnStrip[j]>0);
		SetCtrlAttribute (P->Panel, P->Strips[j], ATTR_XLABEL_VISIBLE,        j==P->NbStrips-1);

		UseLeft=UseRight=FALSE;			// Show the Y axes or not
		for (i=0; i<P->NbTraces; i++) {
			tTrace *TL=&P->TraceList[i];
			if (TL->Axis/2==j and TL->Axis%2==0) UseLeft=TRUE;
			if (TL->Axis/2==j and TL->Axis%2==1) UseRight=TRUE;
		}
		SetCtrlAttribute(P->Panel, P->Strips[j], ATTR_ACTIVE_YAXIS,   VAL_RIGHT_YAXIS);
		SetCtrlAttribute(P->Panel, P->Strips[j], ATTR_YLABEL_VISIBLE, UseRight);
		SetCtrlAttribute(P->Panel, P->Strips[j], ATTR_ACTIVE_YAXIS,   VAL_LEFT_YAXIS);
		SetCtrlAttribute(P->Panel, P->Strips[j], ATTR_YLABEL_VISIBLE, UseLeft);
		#ifdef USE_TSL
			if (PrevNb!=NewNb) {
				ClearStripChart (P->Panel, P->Strips[j]);
				TSL_Reset(P->Panel, P->Strips[j]);
			}
		#endif
		P->NbTracesOnStrip[j]=0;
	}		
	for (i=0; i<P->NbTraces; i++) {
		Tr=&P->TraceList[i];
		if (Tr->Pt==NULL) {
			tTrace *TL=&P->TraceList[i];
			V=ListGetPtrToItem(P->Variables, 1);
			Tr->Pt=V->Pt;	// Set default
			SetCtrlAttribute(P->Panel, TL->CtrlAxis,   ATTR_CTRL_VAL, TL->Axis=P->TraceList[0].Axis);
			SetCtrlAttribute(P->Panel, TL->CtrlSelect, ATTR_CTRL_VAL, 0);
			CallCtrlCallback(P->Panel, TL->CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
			CallCtrlCallback(P->Panel, TL->CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
		}
		
		Strip=MIN(Tr->Axis/2, P->NbStrips-1);
		j = ++P->NbTracesOnStrip[Strip];
		VV.Pt=Tr->Pt;
		Pos =   ListFindItem(P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);
		V = ListGetPtrToItem(P->Variables, (ssize_t)Pos);
		SetTraceAttributeEx(P->Panel, P->Strips[Strip], j, ATTR_TRACE_LG_TEXT, V->Label);
		SetTraceAttributeEx(P->Panel, P->Strips[Strip], j, ATTR_TRACE_YAXIS, 
			Tr->Axis%2==0 ? VAL_LEFT_YAXIS : VAL_RIGHT_YAXIS);
		SetTraceAttributeEx(P->Panel, P->Strips[Strip], j, ATTR_TRACE_LG_VISIBLE, TRUE);
	}
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Add a variable (or change its label if the pointer is already present)
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	Var/Pointer to a variable to display on the strip chart
/// HIPAR	Label/Label associated to the variable (should not be empty)
/// HIRET	0 if no error, 1 if the panel is not a valid GSC panel
///////////////////////////////////////////////////////////////////////////////
int GSC_AddVariable(int panel, double *Var, char* Label) {
	tGscPanel *P=GSC_Find(panel);
	int i, Num;
	size_t Pos;
	tVar *V, VV={NULL, NULL};
	
	if (P==NULL or Var==NULL or Label==NULL) return 1;
	
	VV.Pt=Var;
	Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);
	
	if (Pos>0) {		// Simply change label - Warning: Pos is 1-based
		V=ListGetPtrToItem (P->Variables, (ssize_t)Pos);
		if (NULL==(V->Label=realloc(V->Label, strlen(Label)+1))) return 2;
		strcpy(V->Label, Label);
		for (i=0; i<MAX_TRACES; i++)
			ReplaceListItem(panel, P->TraceList[i].CtrlSelect, (int)Pos-1, Label, Pos-1);
		AdjustSelectWidth(P);
		RebuildCharts(P);	// Overkill
		return 0;
	}

	VV.Pt=Var;
	VV.Label=malloc(strlen(Label)+1);	// Never freed
	strcpy(VV.Label, Label);
	ListInsertItem(P->Variables, &VV, ++P->NbVars);

	for (i=0; i<MAX_TRACES; i++) {
		GetNumListItems(panel, P->TraceList[i].CtrlSelect, &Num);
		InsertListItem (panel, P->TraceList[i].CtrlSelect, P->NbVars-1, Label, P->NbVars-1);
		if (Num==0) {
			SetCtrlIndex    (panel, P->TraceList[i].CtrlSelect, 0);	// Default value if it was empty
			SetCtrlIndex    (panel, P->TraceList[i].CtrlAxis,   3);	// Left Y A
			CallCtrlCallback(panel, P->TraceList[i].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
			CallCtrlCallback(panel, P->TraceList[i].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
		}
	}
	AdjustSelectWidth(P);
	RebuildCharts(P);

	if (P->NbVars==1) {	// Just added the 1st one, reset display
		CallCtrlCallback (panel, GSC_NB_TRACES, EVENT_COMMIT,     0, 0, NULL);
		CallPanelCallback(panel,                EVENT_PANEL_SIZE, 0, 0, NULL);
		SetCtrlAttribute(panel, GSC_NOVAR, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(panel, GSC_FILL,  ATTR_VISIBLE, TRUE);
	}
	
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Remove a variable from the list
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	Var/Pointer to a variable already displayed on the strip chart
/// HIRET	0 if no error, 1 if the panel is not a valid GSC panel, 2 if variable not found
///////////////////////////////////////////////////////////////////////////////
int GSC_DelVariable(int panel, double *Var) {
	tGscPanel *P=GSC_Find(panel);
	tVar *V=NULL, VV={NULL, NULL};
	int i, Idx;
	size_t Pos;
	
	if (P==NULL or Var==NULL) return 1;
	
	VV.Pt=Var;
	Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);
	
	if (Pos==0) return 2;		// Not found - Warning: Pos is 1-based

	V=ListGetPtrToItem(P->Variables, (ssize_t)Pos);
	free(V->Label);
	P->NbVars--;
	ListRemoveItem (P->Variables, 0, (ssize_t)Pos);
	
	for (i=0; i<MAX_TRACES; i++) {
		tTrace *TL=&P->TraceList[i];
		GetCtrlIndex  (panel, TL->CtrlSelect, &Idx);
		DeleteListItem(panel, TL->CtrlSelect, (ssize_t)Pos-1, 1);
		if (Idx==(int)Pos-1) // The ex selection is gone
			CallCtrlCallback(panel, TL->CtrlSelect, EVENT_COMMIT, 0, 0, NULL);
	}
	AdjustSelectWidth(P);

	if (P->NbVars==0) {		// Just removed the last one, reset display
		CallCtrlCallback (panel, GSC_NB_TRACES, EVENT_COMMIT,     0, 0, NULL),
		CallPanelCallback(panel,                EVENT_PANEL_SIZE, 0, 0, NULL);
		SetCtrlAttribute (panel, GSC_NOVAR, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute (panel, GSC_FILL,  ATTR_VISIBLE, FALSE);
	}

	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Panel callback for resizing
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_GenericStripChart (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	tGscPanel *P;
	int i, Height, Width, Top, H=0, ItemsHeight=20;
	
	switch (event) {
		case EVENT_PANEL_SIZE:
			P=GSC_Find(panel);
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);
			GetPanelAttribute(panel, ATTR_WIDTH,  &Width);
			Top = P->CtrlTop + (P->Customize and P->NbVars>0 ? P->CtrlHeight * P->NbTraces : 0);
			if (Height<Top+10*P->NbStrips) 
				SetPanelAttribute(panel, ATTR_HEIGHT, Height=Top+10*P->NbStrips);
			if (P->NbStrips>0) H=(Height-Top-ItemsHeight)/P->NbStrips;
			for (i=0; i<P->NbStrips; i++) {
				SetCtrlAttribute(panel, P->Strips[i], ATTR_HEIGHT, H + (i==P->NbStrips-1 ? ItemsHeight : 0));
				SetCtrlAttribute(panel, P->Strips[i], ATTR_TOP,    Top+i*H);
				SetCtrlAttribute(panel, P->Strips[i], ATTR_WIDTH,  Width);
				SetCtrlAttribute(panel, P->Strips[i], ATTR_LEGEND_LEFT, Width/2);
			}
			PlaceTextMsg(P);
#ifdef USE_TSL
			P->Unit=TSL_Auto(panel, P->Strips[0], P->Rate, TSL_FormatTimeDate, 1.);
#endif
			break;

		case EVENT_GOT_FOCUS:
			P=GSC_Find(panel);
			if (P->NotYet) { P->NotYet=FALSE; AdjustSelectWidth(P); }
			break;
			
		case EVENT_CLOSE:
			HidePanel(panel);	// Note: you need a way to display it back
			break;

#ifdef USE_PM
		case EVENT_KEYPRESS:
			eventData1 &= ~(1<<20);	
			if (HandlePanelDefaultKeys(panel, eventData1)) return 1;
			if (eventData1==(VAL_MENUKEY_MODIFIER  | 'Q') or
				eventData1==(VAL_MENUKEY_MODIFIER  | 'q')) {
				P=GSC_Find(panel);
				ShowAttributes(stdout, panel, 0);
				if (P->NbTraces>0) ShowAttributes(stdout, panel, P->Strips[0]);
				if (P->NbTraces>1) ShowAttributes(stdout, panel, P->Strips[1]);
				return 1;
			}			
			break;
#endif
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change the total number of traces
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_NbTraces (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i, Before;
	tTrace *NewTrace;
	tGscPanel *P;
	tVar *V=NULL;
	BOOL Visible;
	
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			Before=P->NbTraces;
			GetCtrlVal(panel, control, &P->NbTraces);
			SetCtrlAttribute (panel, GSC_TIMER, ATTR_ENABLED, P->NbTraces>0);
			for (i=0; i<MAX_TRACES; i++) {
				Visible = P->Customize and P->NbVars>0 and i<P->NbTraces;
				SetCtrlAttribute(panel, P->TraceList[i].CtrlAxis,   ATTR_VISIBLE, Visible);
				SetCtrlAttribute(panel, P->TraceList[i].CtrlSelect, ATTR_VISIBLE, Visible);
			}
			CallPanelCallback(panel, EVENT_PANEL_SIZE, 0, 0, NULL);

			// If we just added one new control, give it a default value
			if (P->NbTraces>Before and P->NbVars>0) {
				NewTrace=&P->TraceList[P->NbTraces-1];
				NewTrace->Axis=0;								 // Left Y axis of 1st strip by default
				i= (P->NbTraces > P->NbVars ? 0 : P->NbTraces-1);// So we get Var0,1,2...
				V=ListGetPtrToItem(P->Variables, i+1);
				NewTrace->Pt=V->Pt;
				RebuildCharts(P);
				SetCtrlAttribute(panel, NewTrace->CtrlAxis,   ATTR_CTRL_VAL, NewTrace->Axis);
				SetCtrlIndex    (panel, NewTrace->CtrlSelect, i); 
				CallCtrlCallback(panel, NewTrace->CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
				CallCtrlCallback(panel, NewTrace->CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
				CallCtrlCallback(panel, GSC_ALIGN, EVENT_COMMIT, 0, 0, NULL);	// If TRUE
			} else 
				RebuildCharts(P);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Write the duration on the PPS label
///////////////////////////////////////////////////////////////////////////////
static void AdjustDurationLabel(tGscPanel *P) {
	char Str[80];
	double Duration=P->PPS*P->Rate;
	int Left, Width; 
	sprintf(Str, "Pts/chart (%.0f%s)",
		Duration<3*60		? Duration :
		Duration<3*60*60	? Duration/60 :
		Duration<3*60*60*24	? Duration/(60*60) :
							  Duration/(60*60*24),
		Duration<3*60		? "s"   :
		Duration<3*60*60	? "min" :
		Duration<3*60*60*24	? "h"   :
							  "d");
	SetCtrlAttribute(P->Panel, GSC_PPS,         ATTR_LABEL_TEXT, Str);

	GetCtrlAttribute(P->Panel, GSC_PPS,         ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GSC_PPS,         ATTR_LABEL_WIDTH, &Width);
	
	SetCtrlAttribute(P->Panel, GSC_RATE,        ATTR_LEFT,         Left+Width+5);
	GetCtrlAttribute(P->Panel, GSC_RATE,        ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GSC_RATE,        ATTR_LABEL_WIDTH, &Width);

	SetCtrlAttribute(P->Panel, GSC_SCROLL_MODE, ATTR_LEFT,         Left+Width+5);
	GetCtrlAttribute(P->Panel, GSC_SCROLL_MODE, ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GSC_SCROLL_MODE, ATTR_LABEL_WIDTH, &Width);

	SetCtrlAttribute(P->Panel, GSC_ALIGN,       ATTR_LEFT,         Left+Width+5);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Number of points per strip chart
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_PPS (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	int i;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal(panel, control, &P->PPS);
			AdjustDurationLabel(P);
			
			#ifdef USE_TSL
			P->Unit=TSL_Auto(panel, P->Strips[0], P->Rate, TSL_FormatTimeDate, 1.);
			#endif

			for (i=0; i<MAX_STRIPS; i++) {
				SetCtrlAttribute (panel, P->Strips[i], ATTR_POINTS_PER_SCREEN, P->PPS);
				#ifdef USE_TSL
				TSL_Reset(panel, P->Strips[i]);
				#endif
			}
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Timer tick: add points to each strip chart
//  NOTE	If you do not wish to let the timer on auto but instead 
//			you want to update the data only 'when it comes in', proceed as such:
//			Call once  SetCtrlAttribute(YourGscPanel, GSC_TIMER, ATTR_ENABLE,  FALSE);
//			And remove SetCtrlAttribute(YourGscPanel, GSC_RATE,  ATTR_VISIBLE, FALSE);
//			Then when data comes in: CallCtrlCallback(YourGscPanel, GSC_TIMER, EVENT_TIMER_TICK, 0, 0, NULL);
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbt_GSC_Timer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	double Y[MAX_TRACES];
	int i, j, Pos, Nb;
	tGscPanel *P;
	time_t Time;
	switch (event) {
		case EVENT_TIMER_TICK:
			if (NoTimePlots) break;	// Debug
			P=GSC_Find(panel);
			
			if (P->NbVars==0) return 0;	
			Time=time(NULL);
			
			for (j=0; j<P->NbStrips; j++) {
				GetCtrlAttribute (panel, P->Strips[j], ATTR_NUM_TRACES, &Nb);
				Pos=0;
				for (i=0; i<P->NbTraces; i++)
					if (P->TraceList[i].Axis/2==j)
						Y[Pos++] = *P->TraceList[i].Pt;
				if (Pos>0 and P->NbVars>0)
					#ifdef USE_TSL
						TSL_PlotStripChart(panel, P->Strips[j], Y, Pos, 0, 0, VAL_DOUBLE, Time, P->Unit, TSL_FormatTimeDate[P->Unit]);
					#else
						PlotStripChart    (panel, P->Strips[j], Y, Pos, 0, 0, VAL_DOUBLE);
					#endif
			}
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change the left/right axis, or manipulate the order of the traces
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Axis (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	int Tr, i, Do, iSt=0, A, S, S1;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal(panel, control, &Do);
			Tr=P->IndexOfCtrl[control];
			if (Do>0) iSt=Do/2;
			switch (Do>0 ? Do%2 : Do) {
				case 0:
				case 1:
					P->TraceList[Tr].Axis=Do;
					if (eventData2!=-1) RebuildCharts(P);
					break;

				case -3:	// Remove
					for (i=Tr+1; i<P->NbTraces; i++) {
						SetCtrlAttribute(panel, P->TraceList[i-1].CtrlAxis,   ATTR_CTRL_VAL, P->TraceList[i].Axis);
						GetCtrlVal      (panel, P->TraceList[i  ].CtrlSelect,               &S);
						SetCtrlAttribute(panel, P->TraceList[i-1].CtrlSelect, ATTR_CTRL_VAL, S);
					}
					SetCtrlAttribute(panel, GSC_NB_TRACES, ATTR_CTRL_VAL, P->NbTraces-1);
					CallCtrlCallback(panel, GSC_NB_TRACES, EVENT_COMMIT, 0, 0, NULL);
					for (i=Tr; i<P->NbTraces; i++) {
						CallCtrlCallback(panel, P->TraceList[i].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
						CallCtrlCallback(panel, P->TraceList[i].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
					}
					RebuildCharts(P);
					ClearStripChart (panel, P->Strips[iSt]);
					#ifdef USE_TSL
						TSL_Reset(panel, P->Strips[iSt]);
					#endif
					break;

				case -1:	// Move up
					if (Tr<=0) {	// Restore previous value
						SetCtrlAttribute(panel, P->TraceList[0].CtrlAxis,   ATTR_CTRL_VAL, P->TraceList[0].Axis);
						break;
					}
					A=P->TraceList[Tr].Axis;
					GetCtrlVal(panel, P->TraceList[Tr-1].CtrlSelect, &S1);
					GetCtrlVal(panel, P->TraceList[Tr  ].CtrlSelect, &S);
					SetCtrlVal(panel, P->TraceList[Tr  ].CtrlAxis,   P->TraceList[Tr-1].Axis);
					SetCtrlAttribute(panel, P->TraceList[Tr  ].CtrlSelect, ATTR_CTRL_VAL, S1);
					SetCtrlAttribute(panel, P->TraceList[Tr-1].CtrlAxis,   ATTR_CTRL_VAL, A);
					SetCtrlAttribute(panel, P->TraceList[Tr-1].CtrlSelect, ATTR_CTRL_VAL, S);
					CallCtrlCallback(panel, P->TraceList[Tr  ].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr  ].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr-1].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr-1].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
					RebuildCharts(P);
					ClearStripChart (panel, P->Strips[iSt]);
					#ifdef USE_TSL
						TSL_Reset(panel, P->Strips[iSt]);
					#endif
					break;
					
				case -2:	// Move down
					if (Tr>=P->NbTraces-1) {
						SetCtrlAttribute(panel, P->TraceList[P->NbTraces-1].CtrlAxis,   ATTR_CTRL_VAL, P->TraceList[P->NbTraces-1].Axis);
						break;
					}
					A=P->TraceList[Tr].Axis;
					GetCtrlVal(panel, P->TraceList[Tr+1].CtrlSelect, &S1);
					GetCtrlVal(panel, P->TraceList[Tr  ].CtrlSelect, &S);
					SetCtrlAttribute(panel, P->TraceList[Tr  ].CtrlAxis,   ATTR_CTRL_VAL, P->TraceList[Tr+1].Axis);
					SetCtrlAttribute(panel, P->TraceList[Tr  ].CtrlSelect, ATTR_CTRL_VAL, S1);
					SetCtrlAttribute(panel, P->TraceList[Tr+1].CtrlAxis,   ATTR_CTRL_VAL, A);
					SetCtrlAttribute(panel, P->TraceList[Tr+1].CtrlSelect, ATTR_CTRL_VAL, S);
					CallCtrlCallback(panel, P->TraceList[Tr  ].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr  ].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr+1].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
					CallCtrlCallback(panel, P->TraceList[Tr+1].CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
					RebuildCharts(P);
					ClearStripChart (panel, P->Strips[iSt]);
					#ifdef USE_TSL
						TSL_Reset(panel, P->Strips[iSt]);
					#endif
					break;
			}
			break;

#ifdef USE_EASP
		case EVENT_RIGHT_CLICK:
			P=GSC_Find(panel);
			Tr=P->IndexOfCtrl[control];
			iSt=P->TraceList[Tr].Axis/2;
			Do =P->TraceList[Tr].Axis%2;
			EditAxisSettingsPopup(panel, P->Strips[iSt], Do ? VAL_RIGHT_YAXIS : VAL_LEFT_YAXIS, EASP_ALL);
			break;
#endif
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Refresh speed of the traces
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_UpdateRate (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal(panel, control, &P->Rate);
			SetCtrlAttribute (panel, GSC_TIMER, ATTR_INTERVAL, P->Rate);
			AdjustDurationLabel(P);

			#ifdef USE_TSL
			P->Unit=TSL_Auto(panel, P->Strips[0], P->Rate, TSL_FormatTimeDate, 1.);
			#endif
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Select which variable to display
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Select (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Tr, Pos;
	tGscPanel *P;
	tVar *V;
		
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);

			Tr=P->IndexOfCtrl[control];
			GetCtrlIndex(panel, control, &Pos);
			if (Pos==-1) break;	// Empty
			
			V=ListGetPtrToItem(P->Variables, Pos+1);
			P->TraceList[Tr].Pt=V->Pt;
			
			if (eventData2!=-1) RebuildCharts(P);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Actions on the strip charts
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Strip (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i=0, H1, H2, W1=0, W2=0, T1;
	switch (event) {
		case EVENT_LEFT_CLICK:
			GetCtrlAttribute (panel, control, ATTR_LEGEND_VISIBLE, &i);
			SetCtrlAttribute (panel, control, ATTR_LEGEND_VISIBLE, !i);
			break;
		
		case EVENT_LEFT_DOUBLE_CLICK:
//			SetCtrlAttribute (panel, PNL_STRIP, ATTR_ACTIVE_YAXIS,   VAL_LEFT_YAXIS);
			GetAxisScalingMode (panel, control, VAL_LEFT_YAXIS, &i, NULL, NULL);
			SetAxisScalingMode (panel, control, VAL_LEFT_YAXIS, i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			break;

		case EVENT_RIGHT_DOUBLE_CLICK:
//			SetCtrlAttribute (panel, PNL_STRIP, ATTR_ACTIVE_YAXIS,   VAL_RIGHT_YAXIS);
			GetAxisScalingMode (panel, control, VAL_RIGHT_YAXIS, &i, NULL, NULL);
			SetAxisScalingMode (panel, control, VAL_RIGHT_YAXIS, i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			break;

		case EVENT_INTERACTIVE_LEGEND:
			// eventData1 = plot id
			// eventData2 = built-in control menu id (VAL_PLOT_COLOR, etc) */
			switch (eventData2) {
				case VAL_PLOT_COLOR:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_TRACE_COLOR, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				case VAL_PLOT_STYLE:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_PLOT_STYLE, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				case VAL_PLOT_POINT_STYLE:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_TRACE_POINT_STYLE, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				case VAL_PLOT_LINE_STYLE:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_LINE_STYLE, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				case VAL_PLOT_LINE_THICKNESS:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_TRACE_THICKNESS, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				case VAL_PLOT_VISIBLE:
					GetTraceAttribute (panel, control, eventData1+1, ATTR_TRACE_VISIBLE, &i);
					// TODO: do something with it, store it for later, etc...
					break;
				default:break;					
			}
			break;
	}
	
	// Display a temporary indicator
	if (event==EVENT_LEFT_DOUBLE_CLICK or event==EVENT_RIGHT_DOUBLE_CLICK) {
		SetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_CTRL_VAL, 
			i==VAL_AUTOSCALE ? "Axis limits are now locked" : "Axis limits are now set to auto");
		GetCtrlAttribute(panel, control, ATTR_TOP,    &T1);
		GetCtrlAttribute(panel, control, ATTR_HEIGHT, &H1);
		GetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_HEIGHT, &H2);
		SetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_TOP,  T1+(H1-H2)/2);
		if (event==EVENT_RIGHT_DOUBLE_CLICK) {
			GetCtrlAttribute(panel, control, ATTR_WIDTH, &W1);
			GetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_WIDTH, &W2);
		}
		SetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_LEFT, W1-W2);
		SetCtrlAttribute (panel, GSC_AXIS_LIMITS, ATTR_ZPLANE_POSITION, 0);
		SetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(panel, GSC_AXIS_TIMER,  ATTR_ENABLED, TRUE);
	}
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Turn off the temporary indicator
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbt_GSC_AxisTimer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_TIMER_TICK:
			SetCtrlAttribute(panel, control,         ATTR_ENABLED, FALSE);
			SetCtrlAttribute(panel, GSC_AXIS_LIMITS, ATTR_VISIBLE, FALSE);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Number of displayed strip charts
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_NbStrips(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i, j, Idx[MAX_TRACES]={0};
	char Str[80];
	tGscPanel *P;
	
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal(panel, control, &P->NbStrips);
			for (j=0; j<MAX_TRACES;  j++) {
				GetCtrlIndex  (panel, P->TraceList[j].CtrlAxis, &Idx[j]);
				DeleteListItem(panel, P->TraceList[j].CtrlAxis, 3, -1);
			}
			for (i=0; i<MAX_STRIPS; i++) {
				SetCtrlAttribute(panel, P->Strips[i], ATTR_VISIBLE, i<P->NbStrips);
				if (i<P->NbStrips) 
					for (j=0; j<MAX_TRACES; j++) {
						sprintf(Str, "%c Left Y axis", 'A'+i);
						InsertListItem (panel, P->TraceList[j].CtrlAxis, -1, Str, i*2);
						sprintf(Str, "%c Right Y axis", 'A'+i);
						InsertListItem (panel, P->TraceList[j].CtrlAxis, -1, Str, i*2+1);
					}
			}
			for (j=0; j<P->NbTraces; j++) {
				i = (Idx[j]>=P->NbStrips*2+3 ? 3 : Idx[j]);
				SetCtrlIndex(panel, P->TraceList[j].CtrlAxis,  i);
				GetCtrlIndex(panel, P->TraceList[j].CtrlAxis, &i);
				if (i!=Idx[j]) CallCtrlCallback(panel, P->TraceList[j].CtrlAxis, EVENT_COMMIT, 0, -1, NULL);
			}
			RebuildCharts(P);
			CallPanelCallback(panel,            EVENT_PANEL_SIZE, 0, 0, NULL);
			CallCtrlCallback (panel, GSC_ALIGN, EVENT_COMMIT,     0, 0, NULL);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Align the plot areas or leave them in auto
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Align (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	int i, Width, MinWidth=1<<30, Left, FartherRight=0, Height, Fixed;
	
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal (panel, control, &/*P->*/Fixed);
			GetCtrlAttribute(panel, P->Strips[0], ATTR_HEIGHT, &Height); 
			for (i=0; i<P->NbStrips; i++) {
				SetCtrlAttribute (panel, P->Strips[i], ATTR_FIXED_PLOT_AREA, /*P->*/Fixed);
				if (/*P->*/Fixed and P->NbTracesOnStrip[i]>0) {
					GetCtrlAttribute (panel, P->Strips[i], ATTR_PLOT_AREA_WIDTH, &Width);
					GetCtrlAttribute (panel, P->Strips[i], ATTR_PLOT_AREA_LEFT,  &Left);
					if (MinWidth    >Width) MinWidth    =Width;
					if (FartherRight<Left)  FartherRight=Left;
				}
			}
			if (MinWidth<20) MinWidth=20;

			if (/*P->*/Fixed) 
				for (i=0; i<P->NbStrips; i++) {
					SetCtrlAttribute (panel, P->Strips[i], ATTR_PLOT_AREA_WIDTH, MinWidth);
					SetCtrlAttribute (panel, P->Strips[i], ATTR_PLOT_AREA_LEFT,  FartherRight);
				}
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change the scroll mode
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_ScrollMode (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	int i;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal (panel, control, &P->ScrollMode);

			for (i=0; i<MAX_STRIPS; i++) 
				SetCtrlAttribute (panel, P->Strips[i], ATTR_SCROLL_MODE, P->ScrollMode);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Show/Hide the controls that allow customization of the traces
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Customize (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			GetCtrlVal       (panel, control,                     &P->Customize);
			SetCtrlAttribute (panel, GSC_NB_STRIPS,	 ATTR_VISIBLE, P->Customize);
			SetCtrlAttribute (panel, GSC_NB_TRACES,	 ATTR_VISIBLE, P->Customize);
			SetCtrlAttribute (panel, GSC_TEXTMSG,	 ATTR_VISIBLE, P->Customize);
			SetCtrlAttribute (panel, GSC_FILL,	     ATTR_VISIBLE, P->Customize);
//			SetCtrlAttribute (panel, GSC_SCROLL_MODE,ATTR_VISIBLE, P->Customize);
//			SetCtrlAttribute (panel, GSC_ALIGN,   	 ATTR_VISIBLE, P->Customize);
			CallPanelCallback(panel, EVENT_PANEL_SIZE, 0, 0, NULL);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Auto-fill the controls with available variables
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Fill (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGscPanel *P;
	int i, Nb, j;
	switch (event) {
		case EVENT_COMMIT:
			P=GSC_Find(panel);
			if (P->NbVars==0) return 0;
			if (P->NbTraces==0) {	// Use all of them, otherwise use what's available
				SetCtrlAttribute(panel, GSC_NB_TRACES, ATTR_CTRL_VAL, MAX_TRACES-1);
				CallCtrlCallback(panel, GSC_NB_TRACES, EVENT_COMMIT, 0, -1, NULL);
			}
			Nb=MIN(P->NbVars, P->NbTraces);
			GetCtrlIndex(panel, P->TraceList[0].CtrlSelect, &j);
			for (i=0; i<P->NbTraces; i++) {
				tTrace *TL=&P->TraceList[i];
				SetCtrlIndex    (panel, TL->CtrlSelect, j);
				SetCtrlIndex    (panel, TL->CtrlAxis, MIN(2*P->NbStrips-1, 2*MIN(Nb,P->NbStrips)*i/Nb)+3);
				CallCtrlCallback(panel, TL->CtrlSelect, EVENT_COMMIT, 0, -1, NULL);
				CallCtrlCallback(panel, TL->CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
				if (++j >= P->NbVars) j=0;
			}
			RebuildCharts(P);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Hide the panel when click on the window top-right close button
/// HIFN	Note: you need a way to display it back in your main program
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GSC_Hide (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			HidePanel(panel);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Use this to obtain the global characteristics of the charts. See GSC_SetProperties()
/// HIFN	You can pass NULL if you do not wish to obtain a parameter
///////////////////////////////////////////////////////////////////////////////
int GSC_GetProperties(int panel, int *NbStrips, int *NbTraces, int *PPS, double *Rate, 
					  int *ScrollMode /*, int *Fixed*/) {
	tGscPanel *P=GSC_Find(panel);
	if (P==NULL) return 1;

	if (NbStrips  !=NULL) *NbStrips  =P->NbStrips;
	if (NbTraces  !=NULL) *NbTraces  =P->NbTraces;
	if (PPS       !=NULL) *PPS       =P->PPS;
	if (Rate      !=NULL) *Rate      =P->Rate;
	if (ScrollMode!=NULL) *ScrollMode=P->ScrollMode;
//	if (Fixed     !=NULL) *Fixed     =P->Fixed;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Use this to obtain the specific characteristics of a trace
/// HIFN	You can pass NULL if you do not wish to obtain a parameter
/// HIPAR	TraceNb/Loop this parameter from 0 to NbTraces-1 (obtained from GSC_GetProperties)
/// HIPAR	Label/Must have size MaxLabelSize
/// HIPAR	Axis/%2 to get the left/right side, /2 to get the strip chart index
///////////////////////////////////////////////////////////////////////////////
int GSC_GetTraceProperties(int panel, int TraceNb, char* Label, int MaxLabelSize, int *Axis,
	int *PlotColor, int *PlotStyle, int *PlotPointStyle, int *PlotLineStyle, int *PlotLineThickness) {
	int i, C, PosOnSameStrip=1;
	size_t Pos;
	tVar *V, VV={NULL, NULL};
	tGscPanel *P=GSC_Find(panel);
	if (P==NULL or TraceNb<0 or TraceNb>=P->NbTraces) return 1;
	
	if (Label!=NULL) { 
		VV.Pt=P->TraceList[TraceNb].Pt;
		Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);
		V=ListGetPtrToItem (P->Variables, (ssize_t)Pos);
		strncpy(Label, V->Label, (size_t)MaxLabelSize); 
		Label[MaxLabelSize-1]='\0'; 
	}
	if (Axis!=NULL) *Axis=P->TraceList[TraceNb].Axis;
	
	C=P->Strips[P->TraceList[TraceNb].Axis/2];
	for (i=0; i<TraceNb; i++)
		if (P->TraceList[i].Axis/2==P->TraceList[TraceNb].Axis/2) PosOnSameStrip++;
	
	if (PlotColor        !=NULL) GetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_COLOR,       PlotColor);
	if (PlotStyle        !=NULL) GetTraceAttribute(panel, C, PosOnSameStrip, ATTR_PLOT_STYLE,        PlotStyle);
	if (PlotPointStyle   !=NULL) GetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_POINT_STYLE, PlotPointStyle);
	if (PlotLineStyle    !=NULL) GetTraceAttribute(panel, C, PosOnSameStrip, ATTR_LINE_STYLE,        PlotLineStyle);
	if (PlotLineThickness!=NULL) GetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_THICKNESS,   PlotLineThickness);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	You can pass 0 for plot properties if you want to keep the default
/// HIRET	0 if no error, 1 if no matching panel or trace, 2 if no matching variable
/// HIPAR	Label/Must exactly match an existing variable label
///////////////////////////////////////////////////////////////////////////////
int GSC_SetTraceProperties(int panel, int TraceNb, char* Label, int Axis,
		int PlotColor, int PlotStyle, int PlotPointStyle, int PlotLineStyle, int PlotLineThickness) {
	int i, C, PosOnSameStrip=1;
	size_t Pos;
	tVar /**V=NULL,*/ VV={NULL, NULL};
	tGscPanel *P=GSC_Find(panel);
	
	if (P==NULL or TraceNb<0 or TraceNb>=P->NbTraces) return 1;
	if (Label==NULL) return 2;
	
	// Find the variable by matching the string
	VV.Label=Label;
	Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarLabel);

	if (Pos==0) return 2;		// Not found

//	V=ListGetPtrToItem (P->Variables, Pos);
	C=P->Strips[P->TraceList[TraceNb].Axis/2];
	
	SetCtrlIndex    (panel, P->TraceList[TraceNb].CtrlAxis,   Axis+3);
	SetCtrlIndex    (panel, P->TraceList[TraceNb].CtrlSelect, (int)Pos-1);
	CallCtrlCallback(panel, P->TraceList[TraceNb].CtrlAxis,   EVENT_COMMIT, 0, -1, NULL);
	CallCtrlCallback(panel, P->TraceList[TraceNb].CtrlSelect, EVENT_COMMIT, 0,  0, NULL);

	C=P->Strips[P->TraceList[TraceNb].Axis/2];
	for (i=0; i<TraceNb; i++)
		if (P->TraceList[i].Axis/2==P->TraceList[TraceNb].Axis/2) 
			PosOnSameStrip++;
	
	if (PlotColor        !=0) SetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_COLOR,       PlotColor);
	if (PlotStyle        !=0) SetTraceAttribute(panel, C, PosOnSameStrip, ATTR_PLOT_STYLE,        PlotStyle);
	if (PlotPointStyle   !=0) SetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_POINT_STYLE, PlotPointStyle);
	if (PlotLineStyle    !=0) SetTraceAttribute(panel, C, PosOnSameStrip, ATTR_LINE_STYLE,        PlotLineStyle);
	if (PlotLineThickness!=0) SetTraceAttribute(panel, C, PosOnSameStrip, ATTR_TRACE_THICKNESS,   PlotLineThickness);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Use this to obtain the control number of an active strip chart
/// HIRET	The control number of the Nth strip chart or 0 if not found
///////////////////////////////////////////////////////////////////////////////
int GSC_GetStripCtrl(int panel, int Index) {
	tGscPanel *P=GSC_Find(panel);
	if (P==NULL or Index<0 or P->NbStrips<=Index) return 0;
	return P->Strips[Index];
}




///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Test the above code with a bunch of random values in a specific panel
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef GSC_DEBUG

#include "GenericXYplot.h"

static int Pnl, Gsc, Gsc2, Pxy, Pxy2;

#define NB_VAR 15		// How many variables
static double A[NB_VAR];
static char Labels[NB_VAR][80],
	*Unit[NB_VAR]={"", NULL, "Debit (m^3/s)", "Throughput", "Mb/s", "Farts/s", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
static int Ctrl[NB_VAR], IndexOfCtrl[NB_VAR+10];

void AddAllVars(int panel) {
	int i;
	for (i=0; i<NB_VAR; i++)
		GSC_AddVariable(panel,  &A[i], Labels[i]),
		GXY_AddVariable(panel,  &A[i], Labels[i], Unit[i]);
}

///////////////////////////////////////////////////////////////////////////////
void TestConfig(void) {
	int i, Top, Height;
	GetCtrlAttribute(Pnl, PNL_NUMERIC, ATTR_TOP,    &Top);
	GetCtrlAttribute(Pnl, PNL_NUMERIC, ATTR_HEIGHT, &Height);
	for (i=0; i<NB_VAR; i++) {
		sprintf(Labels[i], "Var%c", 'A'+i);
		IndexOfCtrl[ Ctrl[i]=DuplicateCtrl (Pnl, PNL_NUMERIC, Pnl, Labels[i], Top+Height*i, 0) ]=i;
		SetCtrlAttribute(Pnl, Ctrl[i], ATTR_CTRL_VAL, (double)i);
	}
	SetPanelAttribute(Pnl, ATTR_HEIGHT, Top+Height*NB_VAR);
	DiscardCtrl(Pnl, PNL_NUMERIC);
}

///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "GenericStripChartUI.uir", PNL)) < 0) return -1;
	DisplayPanel(Pnl);

	// Strip chart
	if ((Gsc = LoadPanel (0, "GenericStripChartUI.uir", GSC)) < 0) return -1;
	if (GSC_SetProperties(Gsc, 4, 6, 201, 2., 1)) return 1;	// Wrong panel
	DisplayPanel(Gsc);

	if ((Gsc2 = LoadPanel (0, "GenericStripChartUI.uir", GSC)) < 0) return -1;
	SetPanelPos (Gsc2, 20, 800);
	DisplayPanel(Gsc2);

	// XY plot
	if ((Pxy = LoadPanel (0, "GenericStripChartUI.uir", GXY)) < 0) return -1;
	if (GXY_SetProperties(Pxy, 2., 60*60)) return 1;	// Wrong panel
	SetPanelPos (Pxy, 600, 0);
	DisplayPanel(Pxy);

	if ((Pxy2 = LoadPanel (0, "GenericStripChartUI.uir", GXY)) < 0) return -1;
	SetPanelPos (Pxy2, 600, 800);
	DisplayPanel(Pxy2);


	TestConfig();
	AddAllVars(Gsc);
	AddAllVars(Pxy);
	
	RunUserInterface ();

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Randomize (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i;
	switch (event) {
		case EVENT_TIMER_TICK:
			for (i=0; i<NB_VAR; i++) 
				SetCtrlAttribute(panel, Ctrl[i], ATTR_CTRL_VAL, A[i] += (double)(rand())/RAND_MAX-0.5);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ValChange (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Response[80], Str[255];
	int Dimmed;
	
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &A[IndexOfCtrl[control]]);
			break;
		
		case EVENT_LEFT_DOUBLE_CLICK:	// Change label
			GetCtrlAttribute (panel, control, ATTR_LABEL_TEXT, Response);
			sprintf(Str, "Change label of %s control", Response);
			if (0==PromptPopup ("Change label", Str, Response, 79) and strlen(Response)>0) {
				SetCtrlAttribute (panel, control, ATTR_LABEL_TEXT, Response);
				GSC_AddVariable(Gsc,  &A[IndexOfCtrl[control]], Response);
				GXY_AddVariable(Pxy,  &A[IndexOfCtrl[control]], Response, Unit[IndexOfCtrl[control]]);
//				GSC_AddVariable(Gsc2, &A[IndexOfCtrl[control]], Response);
				strcpy(Labels[IndexOfCtrl[control]], Response);
			}
			return 1;

		case EVENT_RIGHT_DOUBLE_CLICK:	// Remove variable from the list
			GetCtrlAttribute(panel, control, ATTR_DIMMED, &Dimmed);
			if (Dimmed) {
				GetCtrlAttribute(panel, control, ATTR_LABEL_TEXT, Str);
				GSC_AddVariable(Gsc,  &A[IndexOfCtrl[control]], Str);
				GXY_AddVariable(Pxy,  &A[IndexOfCtrl[control]], Str, Unit[IndexOfCtrl[control]]);
//				GSC_AddVariable(Gsc2, &A[IndexOfCtrl[control]], Str);
			} else
				GSC_DelVariable(Gsc,  &A[IndexOfCtrl[control]]),
				GXY_DelVariable(Pxy,  &A[IndexOfCtrl[control]]),
//				GSC_DelVariable(Gsc2, &A[IndexOfCtrl[control]]);
			SetCtrlAttribute(panel, control, ATTR_DIMMED, !Dimmed);
			return 1;
		
		default:break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Rate (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	double Rate;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Rate);
			SetCtrlAttribute (panel, PNL_TIMER, ATTR_INTERVAL, Rate);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Test (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int i, Dimmed;
	switch (event) {
		case EVENT_LEFT_DOUBLE_CLICK:	// Display back hidden panels
			DisplayPanel(Gsc);
			DisplayPanel(Gsc2);
			DisplayPanel(Pxy);
			DisplayPanel(Pxy2);
			break;

		case EVENT_RIGHT_DOUBLE_CLICK:	// Reinstate the variables that had been removed
			for (i=0; i<NB_VAR; i++) {
				GetCtrlAttribute(panel, Ctrl[i], ATTR_DIMMED, &Dimmed);
				if (Dimmed) {
					SetCtrlAttribute(panel, Ctrl[i], ATTR_DIMMED, FALSE);
					GSC_AddVariable(Gsc,  &A[i], Labels[i]);
					GSC_AddVariable(Gsc2, &A[i], Labels[i]);
					GXY_AddVariable(Pxy,  &A[i], Labels[i], Unit[i]);
					GXY_AddVariable(Pxy2, &A[i], Labels[i], Unit[i]);
				}
			}
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Copy the configuration from on panel to the next. 
// You could save it to an ini file in between
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Test_Copy (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int NbStrips, NbTraces, PPS, SM;
	double Rate, Keep;
	char Label[80], LabelBottom[80], LabelLeft[80], LabelTop[80], LabelRight[80];
	int i, Axis, PlotColor, PlotStyle, PlotPointStyle, PlotLineStyle, PlotLineThickness;
	int ColorLB,  PointStyleLB,
		ColorRT,  PointStyleRT;

	switch (event) {
		case EVENT_COMMIT:
			GSC_GetProperties(Gsc,  &NbStrips, &NbTraces, &PPS, &Rate, &SM);
			GSC_SetProperties(Gsc2,  NbStrips,  NbTraces,  PPS,  Rate,  SM);
			GXY_GetProperties(Pxy,  &Rate, &Keep);
			GXY_SetProperties(Pxy2,  Rate,  Keep);
			
			AddAllVars(Gsc2);
			AddAllVars(Pxy2);
			
			for (i=0; i<NbTraces; i++) {
				GSC_GetTraceProperties(Gsc, i, Label, 80, &Axis,
					&PlotColor, &PlotStyle, &PlotPointStyle, &PlotLineStyle, &PlotLineThickness);
				printf("%d, %s, %c, %c, %08x, %d, %d, %d, %d\n",
					i, Label, 'A'+Axis/2, Axis%2==0?'L':'R',
					PlotColor,  PlotStyle,  PlotPointStyle,  PlotLineStyle,  PlotLineThickness);
				GSC_SetTraceProperties(Gsc2, i, Label, Axis,
					 PlotColor,  PlotStyle,  PlotPointStyle,  PlotLineStyle,  PlotLineThickness);
			}

			GXY_GetTraceProperties(Pxy, 80, 
				LabelBottom, LabelLeft, LabelTop, LabelRight,
				&ColorLB,  &PointStyleLB,
				&ColorRT,  &PointStyleRT);
			printf("%s/%s, %08x, %d - %s/%s, %08x, %d\n",
				LabelBottom, LabelLeft, ColorLB,  PointStyleLB, 
				LabelTop,    LabelRight,ColorRT,  PointStyleRT);
			GXY_SetTraceProperties(Pxy2,
				LabelBottom, LabelLeft, LabelTop, LabelRight,
				ColorLB,  PointStyleLB,
				ColorRT,  PointStyleRT);
			break;
	}
	return 0;
}

#endif

