///////////////////////////////////////////////////////////////////////////////
// MODULE	GenericXYplot.c
// PURPOSE	Creates one or more panels with 2 pairs of variables displayed on XY plots
// NOTE		Requires the GenericStripChartUI.uir/h files
// AUTHOR	(c) 2012 Guillaume Dargaud - http://www.gdargaud.net/Hack/LabWindows.html
///////////////////////////////////////////////////////////////////////////////


#include <stdlib.h>

#include <cvirte.h>
#include <userint.h>
#include <toolbox.h>


#include "Def.h"
#include "GenericStripChartUI.h"
#include "GenericXYplot.h"

// For testing, see GSC_DEBUG define in GenericStripChart.c

///////////////////////////////////////////////////////////////////////////////
// User settable option
#define USE_PM		// Optional: Panel management routines (printing, etc)
#define USE_EASP	// Optional: Edit axis settings popup
// See http://www.gdargaud.net/Hack/LabWindows.html for the source code to this option
#ifdef USE_PM
	#include "PanelManagement.h"
#endif
#ifdef USE_EASP
	#include "EditAxisSettingsPopup.h"
#endif

int NoXYPlots=FALSE;	// Debug only

///////////////////////////////////////////////////////////////////////////////

// One of the possible variables to trace
typedef struct sVar {
	double *Pt;
	char *Label, *OptAxis;
} tVar;

// Remember past plots - Warning, a massive number will slow things to a crawl.
typedef struct sPlot {
	time_t Time;
	int LB, RT;		// -1 if one is missing
} tPlot;


// One of the possible panels where the generic XY plots are loaded
typedef struct sGxyPanel {
	int Panel, 							// Panel handle when multiple
		NbVars;							// Number of defined variables
	double Rate, Keep;					// In s
	double *Left, *Bottom, *Right, *Top;// Points to values
	ListType Variables, Plots;
	int ColorLB, PointStyleLB;	// Left/bottom plot
	int ColorRT, PointStyleRT;	// Right/top plot
	int KeepUnit;				// 0 for seconds, 1 for minutes, 2 for hours, 3 for days
	BOOL NotYet;				// Skip some lengthy UIR operations in startup phase. Can be removed
} tGxyPanel;

static int NbPanels=0;				// Number of PXY panels in use
static tGxyPanel *GxyPanels=NULL;	// Array of size NbPanels

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Find an already configured GXY panel, or add it to the list and configure it otherwise
/// HIPAR	panel/Refers to an already configured or new GXY panel
/// HIRET	NULL if the panel is not a GXY panel
///////////////////////////////////////////////////////////////////////////////
static tGxyPanel* GXY_Find(int panel) {
	int i;
	tGxyPanel *P;

	if (NoXYPlots) return NULL;

	for (i=0; i<NbPanels; i++)
		if (GxyPanels[i].Panel==panel)
			return &GxyPanels[i];	// We found it already configured

	// Sanity check, make sure we are dealing with a PXY panel
	GetCtrlAttribute (panel, GXY_GRAPH,ATTR_CTRL_STYLE, &i); if (i!=CTRL_GRAPH_LS) return NULL;
	GetCtrlAttribute (panel, GXY_RATE, ATTR_CTRL_STYLE, &i); if (i!=CTRL_NUMERIC_LS) return NULL;
	GetCtrlAttribute (panel, GXY_LEFT, ATTR_CTRL_STYLE, &i); if (i!=CTRL_RECESSED_MENU_RING_LS) return NULL;

	// New PXY panel - Set default values
	if (NULL==(GxyPanels=realloc(GxyPanels, ++NbPanels*sizeof(tGxyPanel)))) return NULL;
	P=&GxyPanels[NbPanels-1];
	P->Panel  =panel;
	P->Rate    =1.;
	P->NbVars  =0;
	P->Left=P->Bottom=P->Right=P->Top=NULL;
	P->Variables=ListCreate(sizeof(tVar));
	P->Plots    =ListCreate(sizeof(tPlot));
	P->KeepUnit=1;
	SetCtrlAttribute(panel, GXY_KEEP, ATTR_LABEL_TEXT, "Keep (min)");
	P->Keep=10*60;
	SetCtrlAttribute(panel, GXY_KEEP, ATTR_CTRL_VAL, (int)(P->Keep/60));
	SetCtrlAttribute(panel, GXY_GRAPH, ATTR_REFRESH_GRAPH, FALSE);
	P->NotYet=TRUE;

#define POINT_STYLES(Ctrl) \
	DeleteListItem(panel, Ctrl, 0, -1);\
	InsertListItem(panel, Ctrl, -1, "Empty square",				VAL_EMPTY_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "Solid square",				VAL_SOLID_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "Asterisk",					VAL_ASTERISK);\
	InsertListItem(panel, Ctrl, -1, "Dotted empty square",		VAL_DOTTED_EMPTY_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "Dotted solid square",		VAL_DOTTED_SOLID_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "Solid diamond",			VAL_SOLID_DIAMOND);\
	InsertListItem(panel, Ctrl, -1, "Empty square with X",		VAL_EMPTY_SQUARE_WITH_X);\
	InsertListItem(panel, Ctrl, -1, "Empty square with cross",	VAL_EMPTY_SQUARE_WITH_CROSS);\
	InsertListItem(panel, Ctrl, -1, "Bold X",					VAL_BOLD_X);\
	InsertListItem(panel, Ctrl, -1, "Small solid square",		VAL_SMALL_SOLID_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "Simple dot",				VAL_SIMPLE_DOT);\
	InsertListItem(panel, Ctrl, -1, "Empty circle",				VAL_EMPTY_CIRCLE);\
	InsertListItem(panel, Ctrl, -1, "Solid circle",				VAL_SOLID_CIRCLE);\
	InsertListItem(panel, Ctrl, -1, "Dotted solid circle",		VAL_DOTTED_SOLID_CIRCLE);\
	InsertListItem(panel, Ctrl, -1, "Dotted empty circle",		VAL_DOTTED_EMPTY_CIRCLE);\
	InsertListItem(panel, Ctrl, -1, "Bold cross",				VAL_BOLD_CROSS);\
	InsertListItem(panel, Ctrl, -1, "Cross",					VAL_CROSS);\
	InsertListItem(panel, Ctrl, -1, "Small cross",				VAL_SMALL_CROSS);\
	InsertListItem(panel, Ctrl, -1, "X",						VAL_X);\
	InsertListItem(panel, Ctrl, -1, "Small X",					VAL_SMALL_X);\
	InsertListItem(panel, Ctrl, -1, "Dotted solid diamond",		VAL_DOTTED_SOLID_DIAMOND);\
	InsertListItem(panel, Ctrl, -1, "Empty diamond",			VAL_EMPTY_DIAMOND);\
	InsertListItem(panel, Ctrl, -1, "Dotted empty diamond",		VAL_DOTTED_EMPTY_DIAMOND);\
	InsertListItem(panel, Ctrl, -1, "Small empty square",		VAL_SMALL_EMPTY_SQUARE);\
	InsertListItem(panel, Ctrl, -1, "No point",					VAL_NO_POINT);

	POINT_STYLES(GXY_POINT_STYLE_LB);
	POINT_STYLES(GXY_POINT_STYLE_RT);

	SetCtrlAttribute(panel, GXY_COLOR_LB,       ATTR_CTRL_VAL, P->ColorLB=VAL_RED);
	SetCtrlAttribute(panel, GXY_COLOR_RT,       ATTR_CTRL_VAL, P->ColorRT=VAL_GREEN);
	SetCtrlAttribute(panel, GXY_POINT_STYLE_LB, ATTR_CTRL_VAL, P->PointStyleLB=VAL_SOLID_SQUARE);
	SetCtrlAttribute(panel, GXY_POINT_STYLE_RT, ATTR_CTRL_VAL, P->PointStyleRT=VAL_X);

	#ifdef USE_EASP
	char Str[500];
	GetCtrlVal(panel, GXY_TEXTMSG, Str);
	strcat(Str, "\nRight-click on var selectors to change axis properties");
	SetCtrlVal(panel, GXY_TEXTMSG, Str);
	#endif

	return P;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init or change the settings on a GXY panel
/// HIFN	Call this after loading the GenericXYplot.uir file into a panel or tab
/// HIFN	Can be called mutiple times to change parameters
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	Rate/Number of updates per second (0 as fast as possible)
/// HIPAR	Keep/Eliminate plot point after this many seconds (0 to disable plotting)
/// HIRET	0 if no error, 1 if the panel is not a valid PXY panel
///////////////////////////////////////////////////////////////////////////////
int GXY_SetProperties(int panel, double Rate, double Keep) {
	tGxyPanel *P=GXY_Find(panel);
	if (P==NULL) return 1;

	SetCtrlAttribute (panel, GXY_RATE,      ATTR_CTRL_VAL, P->Rate=Rate);
	SetCtrlAttribute (panel, GXY_KEEP,      ATTR_CTRL_VAL, (int)(P->Keep=Keep)/
		(P->KeepUnit==0?1:P->KeepUnit==1?60:P->KeepUnit==2?60*60:60*60*24));

	CallCtrlCallback (panel, GXY_RATE,      EVENT_COMMIT,     0, 0, NULL);
	CallCtrlCallback (panel, GXY_KEEP,      EVENT_COMMIT,     0, 0, NULL);
	CallPanelCallback(panel,                EVENT_PANEL_SIZE, 0, 0, NULL);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Adjust the width of the variable selection controls
/// HIFN	Call this when the list of variables grows or shrinks or when a variable is renamed
/// HIPAR   P/GXY panel structure
///////////////////////////////////////////////////////////////////////////////
static void AdjustSelectWidth(tGxyPanel *P) {
	int Width, Left, cLeft;

	// This function is incredibly slow, so we move it someplace where it's called only once
	// NOTE: SizeRingCtrlToText has a bug in CVI<=2010, be sure to modify toolbox.c
	//       See http://forums.ni.com/t5/LabWindows-CVI/SizeRingCtrlToText-not-working/m-p/1916237/highlight/true
	if (!P->NotYet and
		0!=SizeRingCtrlToText(P->Panel, GXY_LEFT)) return;

	GetCtrlAttribute(P->Panel, GXY_LEFT,   ATTR_WIDTH, &Width);
	SetCtrlAttribute(P->Panel, GXY_TOP,    ATTR_WIDTH,  Width);
	SetCtrlAttribute(P->Panel, GXY_RIGHT,  ATTR_WIDTH,  Width);
	SetCtrlAttribute(P->Panel, GXY_BOTTOM, ATTR_WIDTH,  Width);

	GetCtrlAttribute(P->Panel, GXY_BOTTOM,  ATTR_LABEL_WIDTH, &Width);
	GetCtrlAttribute(P->Panel, GXY_BOTTOM,  ATTR_LABEL_LEFT,  &Left);
	GetCtrlAttribute(P->Panel, GXY_DECORATION_LB, ATTR_LEFT,  &cLeft);
	SetCtrlAttribute(P->Panel, GXY_DECORATION_LB, ATTR_WIDTH,  Width+Left-cLeft+5);
	SetCtrlAttribute(P->Panel, GXY_DECORATION_RT, ATTR_WIDTH,  Width+Left-cLeft+5);
	SetCtrlAttribute(P->Panel, GXY_TEXTMSG,       ATTR_LEFT,   Width+Left+10);
}


///////////////////////////////////////////////////////////////////////////////
// Compares the variable pointers (not their values) of list elements
static int CVICALLBACK CompareVarPt(void *item1, void *item2) {
	tVar *I1=item1, *I2=item2;
	return I2->Pt - I1->Pt;	// Test for NULL ?
}

// Compares the labels of list elements
static int CVICALLBACK CompareVarLabel(void *item1, void *item2) {
	tVar *I1=item1, *I2=item2;
	return strcmp(I1->Label, I2->Label);	// Test for NULL ?
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Add a variable (or change its label if the pointer is already present)
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	Var/Pointer to a variable to display on the strip chart
/// HIPAR	Label/Label associated to the variable (should not be empty)
/// HIPAR	OptAxis/Label for the axis (for instance the unit). Size max 30. If NULL, use Label, if "" no label is used
/// HIRET	0 if no error, 1 if the panel is not a valid GXY panel
///////////////////////////////////////////////////////////////////////////////
int GXY_AddVariable(int panel, double *Var, char* Label, char* OptAxis) {
	tGxyPanel *P=GXY_Find(panel);
	int Pos, Num;
	tVar *V, VV={NULL, NULL, NULL};

	if (P==NULL or Var==NULL or Label==NULL) return 1;

	VV.Pt=Var;
	Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);

	if (Pos>0) {		// Simply change label - Warning: Pos is 1-based
		V=ListGetPtrToItem (P->Variables, Pos);
		if (NULL==(V->Label=realloc(V->Label, strlen(Label)+1))) return 2;
		strcpy(V->Label, Label);
		ReplaceListItem(panel, GXY_LEFT,   Pos, Label, Pos);
		ReplaceListItem(panel, GXY_BOTTOM, Pos, Label, Pos);
		ReplaceListItem(panel, GXY_RIGHT,  Pos, Label, Pos);
		ReplaceListItem(panel, GXY_TOP,    Pos, Label, Pos);
		if (P->Left  ==Var) CallCtrlCallback(panel, GXY_LEFT,  EVENT_COMMIT, 0, 0, NULL); // Redraw axis label
		if (P->Bottom==Var) CallCtrlCallback(panel, GXY_BOTTOM,EVENT_COMMIT, 0, 0, NULL);
		if (P->Right ==Var) CallCtrlCallback(panel, GXY_RIGHT, EVENT_COMMIT, 0, 0, NULL);
		if (P->Top   ==Var) CallCtrlCallback(panel, GXY_TOP,   EVENT_COMMIT, 0, 0, NULL);
		AdjustSelectWidth(P);
		return 0;
	}

	VV.Pt=Var;
	VV.Label=malloc(strlen(Label)+1);	// Never freed
	strcpy(VV.Label, Label);
	if (OptAxis!=NULL) {
		int L=strlen(OptAxis);
		if (L>30) L=30;
		VV.OptAxis=malloc(L+1);		// Never freed
		strncpy(VV.OptAxis, OptAxis, L); VV.OptAxis[L]='\0';
	} else VV.OptAxis=NULL;

	ListInsertItem(P->Variables, &VV, ++P->NbVars);

	GetNumListItems(panel, GXY_LEFT,  &Num);
	if (Num==0) {
		InsertListItem (panel, GXY_LEFT,   0, "none", 0);
		InsertListItem (panel, GXY_BOTTOM, 0, "none", 0);
		InsertListItem (panel, GXY_RIGHT,  0, "none", 0);
		InsertListItem (panel, GXY_TOP,    0, "none", 0);
	}
	InsertListItem (panel, GXY_LEFT,   P->NbVars, Label, P->NbVars);
	InsertListItem (panel, GXY_BOTTOM, P->NbVars, Label, P->NbVars);
	InsertListItem (panel, GXY_RIGHT,  P->NbVars, Label, P->NbVars);
	InsertListItem (panel, GXY_TOP,    P->NbVars, Label, P->NbVars);
	AdjustSelectWidth(P);

	if (P->NbVars==1) 	// Just added the 1st one
		SetCtrlAttribute(panel, GXY_NOVAR, ATTR_VISIBLE, FALSE);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Remove a variable from the list
/// HIPAR   panel/Given by LoadPanel, DuplicatePanel...
/// HIPAR	Var/Pointer to a variable already displayed on the strip chart
/// HIRET	0 if no error, 1 if the panel is not a valid GXY panel, 2 if variable not found
///////////////////////////////////////////////////////////////////////////////
int GXY_DelVariable(int panel, double *Var) {
	tGxyPanel *P=GXY_Find(panel);
	tVar *V=NULL, VV={NULL, NULL, NULL};
	int Pos, Idx;

	if (P==NULL or Var==NULL) return 1;

	VV.Pt=Var;
	Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt);

	if (Pos==0) return 2;		// Not found - Warning: Pos is 1-based

	V=ListGetPtrToItem(P->Variables, Pos);
	free(V->Label); V->Label=NULL;
	if (V->OptAxis!=NULL) free(V->OptAxis); V->OptAxis=NULL;
	P->NbVars--;
	ListRemoveItem (P->Variables, 0, Pos);

	GetCtrlIndex  (panel, GXY_LEFT, &Idx);
	DeleteListItem(panel, GXY_LEFT,   Pos, 1);
	if (Idx==Pos) // The ex selection is gone
		CallCtrlCallback(panel, GXY_LEFT,   EVENT_COMMIT, 0, 0, NULL);

	GetCtrlIndex  (panel, GXY_BOTTOM, &Idx);
	DeleteListItem(panel, GXY_BOTTOM,   Pos, 1);
	if (Idx==Pos)
		CallCtrlCallback(panel, GXY_BOTTOM,   EVENT_COMMIT, 0, 0, NULL);

	GetCtrlIndex  (panel, GXY_RIGHT, &Idx);
	DeleteListItem(panel, GXY_RIGHT,   Pos, 1);
	if (Idx==Pos)
		CallCtrlCallback(panel, GXY_RIGHT,   EVENT_COMMIT, 0, 0, NULL);

	GetCtrlIndex  (panel, GXY_TOP, &Idx);
	DeleteListItem(panel, GXY_TOP,   Pos, 1);
	if (Idx==Pos)
		CallCtrlCallback(panel, GXY_TOP,   EVENT_COMMIT, 0, 0, NULL);

	AdjustSelectWidth(P);

	if (P->NbVars==0) 		// Just removed the last one, reset display
		SetCtrlAttribute (panel, GXY_NOVAR, ATTR_VISIBLE, TRUE);

	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Some user interface sanity checks
///////////////////////////////////////////////////////////////////////////////
static void Check(tGxyPanel *P) {
	int Show = P->Keep!=0 and ((P->Left!=NULL and P->Bottom!=NULL) or (P->Right!=NULL and P->Top!=NULL));
	int Nb=(int)(
			P->Keep / (P->Rate==0 ? 0.01 /*arbitrary*/ : P->Rate)
		* ((P->Left!=NULL and P->Bottom!=NULL) + (P->Right!=NULL and P->Top!=NULL))
		);	// 0, 1 or 2
	char Str[80];
	SetCtrlAttribute(P->Panel, GXY_TIMER, ATTR_ENABLED, Show);
	SetCtrlAttribute(P->Panel, GXY_GRAPH, ATTR_DIMMED, !Show);
	if (Nb==0) strcpy(Str, "No data");
	else if (Nb<60*60*2) sprintf(Str, "%d points", Nb);
	else sprintf(Str, "WARNING %dpnts", Nb);
	SetCtrlAttribute(P->Panel, GXY_NB_POINTS, ATTR_CTRL_VAL, Str);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Hide the panel when click on the window top-right close button
/// HIFN	Note: you need a way to display it back in your main program
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Hide (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			HidePanel(panel);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Obtain basic properties for later retrieval
///////////////////////////////////////////////////////////////////////////////
int GXY_GetProperties(int panel, double *Rate, double *Keep) {
	tGxyPanel *P=GXY_Find(panel);
	if (P==NULL) return 1;

	if (Rate!=NULL) *Rate=P->Rate;
	if (Keep!=NULL) *Keep=P->Keep;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Use this to obtain the specific characteristics of the plots
/// HIFN	You can pass NULL if you do not wish to obtain a parameter
/// HIPAR	MaxLabelSize/Common to the various labels
/// HIPAR	LabelBottom/Must have size MaxLabelSize
/// HIPAR	ColorLB/Color of the plot using Left/Bottom axes
/// HIPAR	PointStyleLB/Point style of the plot using Left/Bottom axes
/// HIRET	!=0 if panel not found
///////////////////////////////////////////////////////////////////////////////
int GXY_GetTraceProperties(int panel, int MaxLabelSize,
		char* LabelBottom, char* LabelLeft, char* LabelTop, char* LabelRight,
		int *ColorLB, int *PointStyleLB,
		int *ColorRT, int *PointStyleRT) {
	int Pos;
	tVar *V, VV={NULL, NULL, NULL};
	tGxyPanel *P=GXY_Find(panel);
	if (P==NULL) return 1;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wsign-conversion"
#define GET_LABEL(Side) \
	if (Label##Side!=NULL) {\
		if (P->Side==NULL) Label##Side[0]='\0';\
		else {	/* Find the label in the var list*/ \
			VV.Pt=P->Side; \
			Pos = ListFindItem (P->Variables, &VV, FRONT_OF_LIST, CompareVarPt); \
			V=ListGetPtrToItem (P->Variables, Pos); \
			strncpy(Label##Side, V->Label, MaxLabelSize); \
			Label##Side[MaxLabelSize-1]='\0'; \
		} \
	}
	GET_LABEL(Bottom);
	GET_LABEL(Left);
	GET_LABEL(Top);
	GET_LABEL(Right);
#pragma clang diagnostic pop

	if (ColorLB!=NULL) *ColorLB=P->ColorLB;
	if (ColorRT!=NULL) *ColorRT=P->ColorRT;
	if (PointStyleLB!=NULL) *PointStyleLB=P->PointStyleLB;
	if (PointStyleRT!=NULL) *PointStyleRT=P->PointStyleRT;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Set the specific characteristics of the plots
/// HIFN	You can pass NULL/-1 if you do not wish to set a variable
/// HIPAR	LabelBottom/Must exactly match an existing variable label
/// HIPAR	ColorLB/Color of the plot using Left/Bottom axes, -1 to leave unchanged
/// HIPAR	PointStyleLB/Point style of the plot using Left/Bottom axes, -1 to leave unchanged
/// HIRET	1 if panel not found, 2 if one of the labels wasn't found
///////////////////////////////////////////////////////////////////////////////
int GXY_SetTraceProperties(int panel,
		char* LabelBottom, char* LabelLeft, char* LabelTop, char* LabelRight,
		int ColorLB, int PointStyleLB,
		int ColorRT, int PointStyleRT) {
	int Pos, r=0;
	tVar VV={NULL, NULL, NULL};
	tGxyPanel *P=GXY_Find(panel);
	if (P==NULL) return 1;

	// Find the variable by matching the string
#define SET_LABEL(Side, Ctrl) \
	VV.Label=Label##Side; \
	Pos = ListFindItem(P->Variables, &VV, FRONT_OF_LIST, CompareVarLabel); \
	if (Pos==0) r=2;		/* Not found */	\
	else SetCtrlIndex    (P->Panel, GXY_##Ctrl, Pos), \
		 CallCtrlCallback(P->Panel, GXY_##Ctrl, EVENT_COMMIT, 0, 0, NULL);

	SET_LABEL(Bottom, BOTTOM);
	SET_LABEL(Left,   LEFT);
	SET_LABEL(Top,    TOP);
	SET_LABEL(Right,  RIGHT);

	if (     ColorLB!=-1) SetCtrlAttribute(P->Panel, GXY_COLOR_LB,       ATTR_CTRL_VAL, P->ColorLB=ColorLB);
	if (     ColorRT!=-1) SetCtrlAttribute(P->Panel, GXY_COLOR_RT,       ATTR_CTRL_VAL, P->ColorRT=ColorRT);
	if (PointStyleLB!=-1) SetCtrlAttribute(P->Panel, GXY_POINT_STYLE_LB, ATTR_CTRL_VAL, P->PointStyleLB=PointStyleLB);
	if (PointStyleRT!=-1) SetCtrlAttribute(P->Panel, GXY_POINT_STYLE_RT, ATTR_CTRL_VAL, P->PointStyleRT=PointStyleRT);

	return r;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Panel callback
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_GenericXYplot (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Height, Width, Top;
	tGxyPanel *P;

	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_HEIGHT, &Height);if (Height<100)Height=100;
			GetPanelAttribute(panel, ATTR_WIDTH,  &Width); if (Width<100) Width=100;

			GetCtrlAttribute(panel, GXY_GRAPH, ATTR_TOP,   &Top);
			if (Height<Top+40)
				SetPanelAttribute(panel, ATTR_HEIGHT, Height=Top+40);
			SetCtrlAttribute(panel, GXY_GRAPH, ATTR_HEIGHT, Height-Top);
			SetCtrlAttribute(panel, GXY_GRAPH, ATTR_WIDTH,  Width);
			break;

		case EVENT_GOT_FOCUS:
			P=GXY_Find(panel);
			if (P->NotYet) { P->NotYet=FALSE; AdjustSelectWidth(P); }
			break;

		case EVENT_CLOSE:
			HidePanel(panel);	// Note: you need a way to display it back
			break;

#ifdef USE_PM
		case EVENT_KEYPRESS:
			if (HandlePanelDefaultKeysExt(panel, eventData1, eventData2)) return 1;
			break;
#endif
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Selection of what to display
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Axis(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Pos, Show;
	tGxyPanel *P;
	tVar *V=NULL;
	double *T=NULL;
	char Str[30]="";

	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			if (P==NULL) return 1;

			GetCtrlIndex(panel, control, &Pos);	// 0 is 'none'
			if (Pos>0) {
				V=ListGetPtrToItem(P->Variables, Pos);
				T=V->Pt;
				if (V->OptAxis!=NULL) strcpy(Str, V->OptAxis);
				else { strncpy(Str, V->Label, 29); Str[29]='\0'; }	// 30 chars max for axis name
			}

			switch (control) {
				case GXY_LEFT:   P->Left  =T; break;
				case GXY_BOTTOM: P->Bottom=T; break;
				case GXY_RIGHT:  P->Right =T; break;
				case GXY_TOP:    P->Top   =T; break;
			}

			Show=(P->Left!=NULL and P->Bottom!=NULL);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_XAXIS, VAL_BOTTOM_XAXIS);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_XLABEL_VISIBLE, Show);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_YLABEL_VISIBLE, Show);
			if (control==GXY_LEFT)	 SetCtrlAttribute (panel, GXY_GRAPH, ATTR_YNAME, Str);
			if (control==GXY_BOTTOM) SetCtrlAttribute (panel, GXY_GRAPH, ATTR_XNAME, Str);
//#endif
			Show=(P->Right!=NULL and P->Top!=NULL);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_XLABEL_VISIBLE, Show);
			SetCtrlAttribute (panel, GXY_GRAPH, ATTR_YLABEL_VISIBLE, Show);
			if (control==GXY_RIGHT) SetCtrlAttribute (panel, GXY_GRAPH, ATTR_YNAME, Str);
			if (control==GXY_TOP)   SetCtrlAttribute (panel, GXY_GRAPH, ATTR_XNAME, Str);
//#endif
			Check(P);
			break;

#ifdef USE_EASP
	case EVENT_RIGHT_CLICK:
			switch (control) {
				case GXY_BOTTOM: EditAxisSettingsPopup(panel, GXY_GRAPH, VAL_BOTTOM_XAXIS, EASP_ALL); break;
				case GXY_LEFT:   EditAxisSettingsPopup(panel, GXY_GRAPH, VAL_LEFT_YAXIS,   EASP_ALL); break;
				case GXY_TOP:    EditAxisSettingsPopup(panel, GXY_GRAPH, VAL_TOP_XAXIS,    EASP_ALL); break;
				case GXY_RIGHT:  EditAxisSettingsPopup(panel, GXY_GRAPH, VAL_RIGHT_YAXIS,  EASP_ALL); break;
			}
			break;
#endif
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Update rate of the plot. 0 for as fast as possible
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_UpdateRate (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			GetCtrlVal(panel, control, &P->Rate);
			SetCtrlAttribute (panel, GXY_TIMER, ATTR_INTERVAL, P->Rate);
			Check(P);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Quantity of data to keep on the plot. Massive resource hog, may want to limit
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Keep (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	int K;
	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			GetCtrlVal(panel, control, &K);

			#define ReLabel SetCtrlAttribute(panel, control, ATTR_LABEL_TEXT, \
				P->KeepUnit==0 ? "Keep (s)" : P->KeepUnit==1 ? "Keep (min)" : P->KeepUnit==2 ? "Keep (h)" : "Keep (days)" );

			switch (P->KeepUnit) {
				case 0:	P->Keep=K;
							 if (K>=3*60) { P->KeepUnit++; SetCtrlVal(panel, control, K/60); ReLabel; }
						break;
				case 1:	P->Keep=K*60; 	// Convert to seconds
							 if (K<3)     { P->KeepUnit--; SetCtrlVal(panel, control, K*60); ReLabel; }
						else if (K>=3*60) { P->KeepUnit++; SetCtrlVal(panel, control, K/60); ReLabel; }
						break;
				case 2:	P->Keep=K*60*60;
							 if (K<3)     { P->KeepUnit--; SetCtrlVal(panel, control, K*60); ReLabel; }
						else if (K>=3*24) { P->KeepUnit++; SetCtrlVal(panel, control, K/24); ReLabel; }
						break;
				case 3:	P->Keep=K*60*60*24;
							 if (K<3)     { P->KeepUnit--; SetCtrlVal(panel, control, K*24); ReLabel; }
						break;
			}

			Check(P);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Clear the plots
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Clear (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			DeleteGraphPlot (panel, GXY_GRAPH, -1, VAL_IMMEDIATE_DRAW);
			ListClear(P->Plots);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change plot point style
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_PointStyle (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			GetCtrlVal(panel, control, control==GXY_POINT_STYLE_LB ? &P->PointStyleLB : &P->PointStyleRT);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Change plot color
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Color (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	switch (event) {
		case EVENT_COMMIT:
			P=GXY_Find(panel);
			GetCtrlVal(panel, control, control==GXY_COLOR_LB ? &P->ColorLB : &P->ColorRT);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Various graph properties changes
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_GXY_Graph (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i;
	switch (event) {
		case EVENT_LEFT_DOUBLE_CLICK:
			GetAxisScalingMode (panel, control, VAL_LEFT_YAXIS,  &i, NULL, NULL);
			SetAxisScalingMode (panel, control, VAL_LEFT_YAXIS,   i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			SetAxisScalingMode (panel, control, VAL_BOTTOM_XAXIS, i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			break;

		case EVENT_RIGHT_DOUBLE_CLICK:
			GetAxisScalingMode (panel, control, VAL_RIGHT_YAXIS, &i, NULL, NULL);
			SetAxisScalingMode (panel, control, VAL_RIGHT_YAXIS,  i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			SetAxisScalingMode (panel, control, VAL_TOP_XAXIS,    i==VAL_AUTOSCALE ? VAL_LOCK : VAL_AUTOSCALE, 0.0, 0.0);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Timer tick: add points to the graph.
//  NOTE	If you do not wish to let the timer on auto but instead
//			you want to update the data only 'when it comes in', proceed as such:
//			Call once  SetCtrlAttribute(YourGxyPanel, GXY_TIMER, ATTR_ENABLE,  FALSE);
//			And remove SetCtrlAttribute(YourGxyPanel, GXY_RATE,  ATTR_VISIBLE, FALSE);
//			Then when data comes in: CallCtrlCallback(YourGxyPanel, GXY_TIMER, EVENT_TIMER_TICK, 0, 0, NULL);
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbt_GXY_Timer (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	tGxyPanel *P;
	tPlot *Ptr, Plot={0, -1, -1};

	switch (event) {
		case EVENT_TIMER_TICK:
			if (NoXYPlots) break;	// Debug
			P=GXY_Find(panel);

			if (P->NbVars==0) return 0;
			Plot.Time=time(NULL);

			// Delete plots older than Keep seconds - Act as a FIFO
			while (P->Keep>0 /*and ListNumItems(P->Plots)>0*/) {
				Ptr=ListGetPtrToItem (P->Plots, FRONT_OF_LIST);
				if (Ptr==NULL or abs((int)(difftime(Ptr->Time, Plot.Time)))<P->Keep) break;
				if (Ptr->LB!=-1) DeleteGraphPlot(panel, GXY_GRAPH, Ptr->LB, VAL_NO_DRAW);
				if (Ptr->RT!=-1) DeleteGraphPlot(panel, GXY_GRAPH, Ptr->RT, VAL_NO_DRAW);
				ListRemoveItem (P->Plots, 0, FRONT_OF_LIST);
			}

			if (P->Left!=NULL and P->Bottom!=NULL) {
				SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_XAXIS, VAL_BOTTOM_XAXIS);
				SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_YAXIS, VAL_LEFT_YAXIS);
				Plot.LB=PlotPoint(panel, GXY_GRAPH, *P->Bottom, *P->Left, P->PointStyleLB, P->ColorLB);
			}
			if (P->Right!=NULL and P->Top!=NULL) {
				SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_XAXIS, VAL_TOP_XAXIS);
				SetCtrlAttribute (panel, GXY_GRAPH, ATTR_ACTIVE_YAXIS, VAL_RIGHT_YAXIS);
				Plot.RT=PlotPoint(panel, GXY_GRAPH, *P->Top, *P->Right, P->PointStyleRT, P->ColorRT);
			}
			if (Plot.LB!=-1 and Plot.RT!=-1)
				ListInsertItem (P->Plots, &Plot, END_OF_LIST);

			if (P->Rate>=1.) RefreshGraph(panel, GXY_GRAPH);
			break;
	}
	return 0;
}
