#ifndef __GENERIC_XY_PLOT_H
#define __GENERIC_XY_PLOT_H

// For simple use, simply load the GXY panel from GenericStripChartUI.uir
// Then call GXY_SetProperties, then call GXY_AddVariable as many times as needed. Done.

extern int GXY_SetProperties(int panel, double Rate, double Keep);

extern int GXY_AddVariable(int panel, double *Var, char* Label, char* OptAxis);

extern int GXY_DelVariable(int panel, double *Var);

// Use the following to save the status of all the traces: 
// First GXY_GetProperties, then GXY_GetTraceProperties
// To reload a saved status:
// Call GXY_SetProperties, 
// then AddVariable with the same variables as before, 
// then SetTraceProperties
// See the example in the main() of GenericStripChart.c

extern int GXY_GetProperties(int panel, double *Rate, double *Keep);

extern int GXY_GetTraceProperties(int panel, int MaxLabelSize,
	char* LabelBottom, char* LabelLeft, char* LabelTop, char* LabelRight, 
	int *ColorLB, int *PointStyleLB, 
	int *ColorRT, int *PointStyleRT);

extern int GXY_SetTraceProperties(int panel, 
	char* LabelBottom, char* LabelLeft, char* LabelTop, char* LabelRight,
	int ColorLB, int PointStyleLB, 
	int ColorRT, int PointStyleRT);

extern BOOL NoXYPlots;	// Debug only

#endif
