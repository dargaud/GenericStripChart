/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  GSC                              1       /* callback function: cbp_GenericStripChart */
#define  GSC_CUSTOM                       2       /* control type: radioButton, callback function: cb_GSC_Customize */
#define  GSC_PPS                          3       /* control type: numeric, callback function: cb_GSC_PPS */
#define  GSC_RATE                         4       /* control type: numeric, callback function: cb_GSC_UpdateRate */
#define  GSC_SCROLL_MODE                  5       /* control type: ring, callback function: cb_GSC_ScrollMode */
#define  GSC_ALIGN                        6       /* control type: radioButton, callback function: cb_GSC_Align */
#define  GSC_AXIS                         7       /* control type: ring, callback function: cb_GSC_Axis */
#define  GSC_SELECT                       8       /* control type: ring, callback function: cb_GSC_Select */
#define  GSC_NB_STRIPS                    9       /* control type: numeric, callback function: cb_GSC_NbStrips */
#define  GSC_NB_TRACES                    10      /* control type: numeric, callback function: cb_GSC_NbTraces */
#define  GSC_FILL                         11      /* control type: command, callback function: cb_GSC_Fill */
#define  GSC_STRIP                        12      /* control type: strip, callback function: cb_GSC_Strip */
#define  GSC_HIDE                         13      /* control type: command, callback function: cb_GSC_Hide */
#define  GSC_AXIS_TIMER                   14      /* control type: timer, callback function: cbt_GSC_AxisTimer */
#define  GSC_TIMER                        15      /* control type: timer, callback function: cbt_GSC_Timer */
#define  GSC_COPYRIGHT                    16      /* control type: textMsg, callback function: (none) */
#define  GSC_TEXTMSG                      17      /* control type: textMsg, callback function: (none) */
#define  GSC_AXIS_LIMITS                  18      /* control type: textMsg, callback function: (none) */
#define  GSC_NOVAR                        19      /* control type: textMsg, callback function: (none) */

#define  GXY                              2       /* callback function: cbp_GenericXYplot */
#define  GXY_RATE                         2       /* control type: numeric, callback function: cb_GXY_UpdateRate */
#define  GXY_KEEP                         3       /* control type: numeric, callback function: cb_GXY_Keep */
#define  GXY_CLEAR                        4       /* control type: command, callback function: cb_GXY_Clear */
#define  GXY_NB_POINTS                    5       /* control type: textMsg, callback function: (none) */
#define  GXY_POINT_STYLE_LB               6       /* control type: ring, callback function: cb_GXY_PointStyle */
#define  GXY_COLOR_LB                     7       /* control type: color, callback function: cb_GXY_Color */
#define  GXY_BOTTOM                       8       /* control type: ring, callback function: cb_GXY_Axis */
#define  GXY_LEFT                         9       /* control type: ring, callback function: cb_GXY_Axis */
#define  GXY_POINT_STYLE_RT               10      /* control type: ring, callback function: cb_GXY_PointStyle */
#define  GXY_COLOR_RT                     11      /* control type: color, callback function: cb_GXY_Color */
#define  GXY_TOP                          12      /* control type: ring, callback function: cb_GXY_Axis */
#define  GXY_RIGHT                        13      /* control type: ring, callback function: cb_GXY_Axis */
#define  GXY_HIDE                         14      /* control type: command, callback function: cb_GXY_Hide */
#define  GXY_TIMER                        15      /* control type: timer, callback function: cbt_GXY_Timer */
#define  GXY_COPYRIGHT                    16      /* control type: textMsg, callback function: (none) */
#define  GXY_TEXTMSG                      17      /* control type: textMsg, callback function: (none) */
#define  GXY_GRAPH                        18      /* control type: graph, callback function: cb_GXY_Graph */
#define  GXY_NOVAR                        19      /* control type: textMsg, callback function: (none) */
#define  GXY_DECORATION_RT                20      /* control type: deco, callback function: (none) */
#define  GXY_DECORATION_LB                21      /* control type: deco, callback function: (none) */

#define  PNL                              3       /* callback function: cbp_Test */
#define  PNL_QUIT                         2       /* control type: command, callback function: cb_Quit */
#define  PNL_NUMERIC                      3       /* control type: numeric, callback function: cb_ValChange */
#define  PNL_TIMER                        4       /* control type: timer, callback function: cb_Randomize */
#define  PNL_RATE                         5       /* control type: numeric, callback function: cb_Rate */
#define  PNL_COPY                         6       /* control type: command, callback function: cb_Test_Copy */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cb_GSC_Align(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Axis(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Customize(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Fill(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Hide(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_NbStrips(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_NbTraces(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_PPS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_ScrollMode(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Select(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_Strip(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GSC_UpdateRate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Axis(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Clear(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Color(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Graph(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Hide(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_Keep(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_PointStyle(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_GXY_UpdateRate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Randomize(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Rate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Test_Copy(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ValChange(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_GenericStripChart(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_GenericXYplot(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Test(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbt_GSC_AxisTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbt_GSC_Timer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbt_GXY_Timer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
