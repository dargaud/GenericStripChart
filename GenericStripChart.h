#ifndef __GENERIC_STRIP_CHART_H
#define __GENERIC_STRIP_CHART_H

// For simple use, simply load the GSC panel from GenericStripChartUI.uir
// Then call GSC_SetProperties, then call GSC_AddVariable as many times as needed. Done.

extern int GSC_SetProperties(int panel, int NbStrips, int NbTraces, int PPS, 
							 double Rate, int ScrollMode /*, int Fixed*/);

extern int GSC_AddVariable(int panel, double *Var, char* Label);

extern int GSC_DelVariable(int panel, double *Var);

// Use the following to save the status of all the traces: 
// First GSC_GetProperties, 
// then GSC_GetTraceProperties looping on NbTraces
// To reload a saved status:
// Call GSC_SetProperties, 
// then AddVariable with the same variables as before, 
// then SetTraceProperties looping on NbTraces
// See the example in 
extern int GSC_GetProperties(int panel, int *NbStrips, int *NbTraces, int *PPS, 
							 double *Rate, int *ScrollMode /*, int *Fixed*/);

extern int GSC_GetTraceProperties(int panel, int TraceNb, char* Label, int MaxLabelSize, int *Axis,
	int *PlotColor, int *PlotStyle, int *PlotPointStyle, int *PlotLineStyle, int *PlotLineThickness);

extern int GSC_SetTraceProperties(int panel, int TraceNb, char* Label, int Axis,
	int PlotColor, int PlotStyle, int PlotPointStyle, int PlotLineStyle, int PlotLineThickness);

extern int GSC_GetStripCtrl(int panel, int Index);

extern int NoTimePlots;	// Debug only

#endif
