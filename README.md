Generic Strip Charts and generic X/Y plots in LabWindows/CVI.  
Meaning you can associate arbitrary internal program variables and follow them with time, letting the user customize his own display.  

Use the following options either in [Options][Buils Options][Compiler Defines]  
or as #define at the start (see the code): 

/DGSC_DEBUG  
Creates a test program for both Generic Strip Charts and XY plots

#define USE_TSL  
Use time strip labels.

#define USE_PM  
Panel management routines (printing, etc)

#define USE_EASP  	
Edit axis settings popup

See http://www.gdargaud.net/Hack/LabWindows.html#GSC for the source code to those 3 USE options
